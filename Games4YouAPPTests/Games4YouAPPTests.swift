//
//  Games4YouAPPTests.swift
//  Games4YouAPPTests
//
//  Created by José Pedro Nascimento Guerra on 26/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import XCTest
import GoogleSignIn
import Cloudinary
@testable import Games4YouAPP

class Games4YouAPPTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLoginBadCredencials(){
        let expectation = XCTestExpectation()
        
        Autentication.login(username: "root", password: "root") { (user, err) in
            XCTAssertNil(user)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
    }
    func testLoginCorrectCredencials(){
        let expectation = XCTestExpectation()
        
        Autentication.login(username: "piteu", password: "123") { (user, err) in
            XCTAssertNotNil(user)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
    }
    func testRegisterNewUser(){
        let expectation = XCTestExpectation()
        //teste so passa uma vez porque cria o user e ja nao o pode registar
        Autentication.register(username: "teste", password: "teste", email: "teste@gmail.com") { (user, error) in
            XCTAssertNotNil(user)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testApiIGBD(){
        let expectation = XCTestExpectation()
        
        apiIgbd.getTop { (games, error) in
            XCTAssertNotNil(games)
            XCTAssertEqual(games?.count, 10)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 30)
    }
    func testsearchGames(){
        let expectation = XCTestExpectation()
        
        apiIgbd.getSearchedGames("fifa f") { (ids, error) in
            XCTAssertNotNil(ids)
            XCTAssertEqual(ids?.count, 10)
            expectation.fulfill()
        }
        
        
        wait(for: [expectation], timeout: 30)
    }
    
    func testCommentGame(){
        let expectation = XCTestExpectation()
        
        Game.addComentario(texto: "comentario teste", idJogo:"27081" , idUtilizador: "2", gameName:"FIFA 18") { (id,error ) in
            XCTAssertNotEqual(id,0)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 30)
    }
    
    func testCommentGameButNoLogin(){
        let expectation = XCTestExpectation()
        
        Game.addComentario(texto: "comentario teste", idJogo:"27081" , idUtilizador: "0", gameName:"FIFA 18") { (id,error ) in
            XCTAssertEqual(id,0)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 30)
    }
    func testAddGameToFavorites(){
        let expectation = XCTestExpectation()
        
        Game.alterGame(idUtilizador: "2", idJogo: "7329", meuJogo: "1", favorito: "1", name: "Dead Island 2", coverUrl: "https://igdb.spacechop.com/igdb/image/upload/t_thumb/lumgkti6rht3evlbu8xw.jpg") { (error) in
            XCTAssertNil(error)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 30)
    }
    
}

