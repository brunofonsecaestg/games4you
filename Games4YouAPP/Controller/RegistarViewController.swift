//
//  RegistarViewController.swift
//  Games4YouAPP
//
//  Created by formando on 15/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class RegistarViewController: UIViewController {

    @IBOutlet weak var textUsername: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textConfirmPassword: UITextField!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textError: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
  

    @IBAction func registarButton(_ sender: UIButton) {
        var valido = true
     
        if textUsername.text == "" || textUsername.text == nil {
            textError.text = "Credenciais invalidas"
            valido = false
        }
        if textPassword.text == "" || textPassword.text == nil {
            textError.text = "Credenciais invalidas"
            valido = false
        }
        if textConfirmPassword.text == "" || textConfirmPassword.text == nil {
            textError.text = "Credenciais invalidas"
            valido = false
        }
        if textEmail.text == "" || textEmail.text == nil {
            textError.text = "Credenciais invalidas"
            valido = false
        }
        if textPassword.text != textConfirmPassword.text {
            textError.text = "Credenciais invalidas"
            valido = false
        }

        if valido == true {
            Autentication.register(username: textUsername.text!, password: textPassword.text!,email: textEmail.text!, completion: { (user, error) in
                if user != nil {
                    
                    self.textError.text = ""
                    DispatchQueue.main.sync {
                        let alert = UIAlertController(title: "Games4You", message: "\(self.textUsername.text!), Criou uma conta e esta logado!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                            { action in
                                self.performSegue(withIdentifier: "registerSegue", sender: nil)
                            }
                        ))
                        self.present(alert, animated: true, completion: nil)
                    }
                    Autentication.login(username: self.textUsername.text!, password: self.textPassword.text!, completion: { (user, error) in
                        Repositorio.repositorio.users.append(user!)
                        Repositorio.repositorio.guardarUser()
                    })
                   //  self.performSegue(withIdentifier: "mySegue", sender: nil)
                } else {
                    DispatchQueue.main.async {
                        self.textError.text = ""
                        let alert = UIAlertController(title: "Games4You", message: "ja existe um utilizador com esse username", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
            
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination as? PesquisaJogosViewController
        segue.destination.navigationItem.hidesBackButton = true
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
