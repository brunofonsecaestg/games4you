//
//  GameInfoViewController.swift
//  Games4YouAPP
//
//  Created by Admin on 09/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
import WebKit

class GameInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  ,UITextFieldDelegate {
 
 
    
    var game:Game!
    var user:User!
    var c:Comentario!


    @IBOutlet weak var navBar: UINavigationItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!

    
 
    @IBOutlet weak var tbData: UITableView!
    
   
    
    let playersVars = ["origin":"https://www.youtube.com"]
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var comentarios:[Comentario] = []
    
    @IBOutlet weak var textNoStream: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tbData.delegate = self
        tbData.dataSource = self
        navBar.title = game.name
        
       // addToolBar(textField: textComent)

        self.tbData.rowHeight = UITableViewAutomaticDimension;
        self.tbData.estimatedRowHeight = 44.0; // set to whatever your "average" cell height is

        Game.getComentarios(idJogo: String(describing: game.id!), completion: { (coms,error) in
            self.comentarios = coms
            self.game.comentarios = self.comentarios
            DispatchQueue.main.async {
                self.tbData.reloadData()
            }
        })
        
      game.comentarios = comentarios
        
     
        if (Repositorio.repositorio.users.count != 0){
            user = Repositorio.repositorio.users[0]
        }
        
        
        registerForKeyboardNotifications()
        

//
//
//
//        if game.twitchChannels?.count == 0 {
//            textNoStream.isHidden = false
//        }
//        if game.videos?.count == 0 {
//            textNoVideo.isHidden = false
//        }
//
//        if NetworkConnection.isConnectedToInternet() == false {
//            textNoVideo.text = "No Internet Connection"
//            textNoStream.text = "No Internet Connection"
//            textNoVideo.isHidden = false
//            textNoStream.isHidden = false
//        }else {
//            self.VideoCollectionView.delegate = self
//            self.VideoCollectionView.dataSource = self
//            self.TwichCollectionPlayer.delegate = self
//            self.TwichCollectionPlayer.dataSource = self
//        }
//
//        navBar.title = game.name
//        textLabel.text = game.desc
//        textDate.text = game.first_release_date
//        textDate.sizeToFit()
//
//        if game.coverImage != nil {
//        gameImage.image = game.coverImage
//        } else{
//            gameImage.image = #imageLiteral(resourceName: "gameImageDefault")
//        }
//
//
//        if game.rating != nil {
//        textRating.text = "\(game.rating!.rounded())"
//        }else {
//            textRating.text = "N/A"
//        }
//
//        if self.revealViewController() != nil {
//           menuButton.target = self.revealViewController()
//            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }
        // Do any additional setup after loading the view.
       // myGameButton.setImage(#imageLiteral(resourceName: "remove"), for:UIControlState.highlighted)
    }


 
    func registerForKeyboardNotifications()
    {
       // NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
      //  NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillBeShown(note:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillBeHidden(note:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
   

    
    @objc func keyboardWillBeShown(note: Notification) {
        let userInfo = note.userInfo
        let keyboardFrame = userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect
        let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.height, 0.0)
        self.tbData.contentInset = contentInset
        self.tbData.scrollIndicatorInsets = contentInset
        self.tbData.scrollRectToVisible(keyboardFrame, animated: true)
    }
    
    @objc func keyboardWillBeHidden(note: Notification) {
        let contentInset = UIEdgeInsets.zero
        self.tbData.contentInset = contentInset
        self.tbData.scrollIndicatorInsets = contentInset
    }
    /*
    // MARK: - Navigation
*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((game.comentarios?.count)!+4)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "gameInfoCell", for: indexPath) as! GameInfoTableViewCell
         
            cell.game = game
            if user != nil {
                cell.user = user
                Game.getGameEstado(idUtilizador: String(user.id!), idJogo: String(describing: game.id!), completion: { (favorito,meuJogo,error) in
                    if favorito == 1 {
                        DispatchQueue.main.async {
                            cell.favoritesButton.setImage(#imageLiteral(resourceName: "isFavorite"), for: .normal)
                        }
                    }
                    if meuJogo == 1 {
                        DispatchQueue.main.async {
                            cell.myGamesButton.setImage(#imageLiteral(resourceName: "remove") ,for: .normal)
                        }
                    }
                })
            }
            cell.gameViewController = self
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath) as! VideosTableViewCell
           
            cell.game = game
            cell.VideoCollectionView.delegate = cell as! UICollectionViewDelegate
            cell.VideoCollectionView.dataSource = cell as! UICollectionViewDataSource
            //cell.gameViewController = self
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "twitchCell", for: indexPath) as! TwitchTableViewCell
            
            cell.game = game
            cell.TwitchCollectionView.delegate = cell as! UICollectionViewDelegate
            cell.TwitchCollectionView.dataSource = cell as! UICollectionViewDataSource
            cell.selectionStyle = .none
            return cell
            
        }
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "descricaoCell", for: indexPath) as! DescricaoTableViewCell
            
            cell.game = game
            cell.user = user
            cell.gameViewController = self
         
            cell.addToolBar(textField: cell.comentarioText)
            cell.selectionStyle = .none
            return cell
        }
        else
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comentarioCell", for: indexPath) as! ComentarioTableViewCell
        let comentario = game.comentarios![indexPath.row-4]
        cell.comentario = comentario
        cell.gameViewController = self
             return cell
        }
    }
    
    

 
    func removeComentario() {
        Game.getComentarios(idJogo: String(describing: game.id!), completion: { (coms,error) in
            self.comentarios = coms
            self.game.comentarios = self.comentarios
            DispatchQueue.main.async {
                self.tbData.reloadData()
            }
        })
        game.comentarios = comentarios
    }
    


}
