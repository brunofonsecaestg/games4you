//
//  GerirJogosViewController.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 05/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class GerirJogosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var segmentedButton: UISegmentedControl!
    var segmentedStatus:Int = 0
    
    @IBAction func segmentedButtonAction(_ sender: Any) {
        segmentedStatus = segmentedButton.selectedSegmentIndex
        updateMyGamesAndFavorites()
    }
    
    var myGames = [[String:Any]]()
    var favorites = [[String:Any]]()

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateMyGamesAndFavorites()
    }

    func updateMyGamesAndFavorites() {
        print("UPDATE")
        myGames = []
        favorites = []
        var user_id:String = String(describing: Repositorio.repositorio.users[0].id!)
        
        appDelegate.myGames = []
        appDelegate.favorites = []
        tableView.reloadData()
        
        Autentication.getMyGamesAndFavorites(user_id: user_id, completion: {(myGames, favorites, error) in
            if myGames != nil {
                self.appDelegate.myGames = []
                for game in myGames!{
                    var game = game
                    if(game["cover"] as? String == nil || game["cover"] as? String == "N/A"){
                        self.myGames.append(game)
                        self.appDelegate.myGames.append(game)
                         DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                     }else{
                         Game.getGameImage((game["cover"] as! String), completion: { (gameImage, error) in
                             game["coverImage"] = gameImage
                             self.myGames.append(game)
                            self.appDelegate.myGames.append(game)
                             DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        })
                     }
                }
            }else{
                self.appDelegate.myGames = []
                self.tableView.reloadData()
            }
            
            if favorites != nil {
                self.appDelegate.favorites = []
                for game in favorites!{
                    var game = game
                    if(game["cover"] as? String == nil || game["cover"] as? String == "N/A"){
                        self.favorites.append(game)
                        self.appDelegate.favorites.append(game)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }else{
                        Game.getGameImage((game["cover"] as! String), completion: { (gameImage, error) in
                            game["coverImage"] = gameImage
                            self.favorites.append(game)
                            self.appDelegate.favorites.append(game)
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        })
                    }
                }
            }
            else{
                self.appDelegate.favorites = []
                self.tableView.reloadData()
            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(segmentedStatus == 0 && myGames != nil){
            return myGames.count
        }
        if(segmentedStatus == 1 && favorites != nil){
            return favorites.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameCellWithRemove", for: indexPath) as! GameWithRemoveViewCell
        
        // Configure the cell...
        
        if(segmentedStatus == 0 && myGames != nil){
            let game = myGames[indexPath.row]
            cell.game = game
            cell.removeButton.setImage(#imageLiteral(resourceName: "remove"), for: UIControlState.normal)
            cell.mode = "myGames"
        }
        if(segmentedStatus == 1 && favorites != nil){
            let game = favorites[indexPath.row]
            cell.mode = "favorites"
            cell.removeButton.setImage(#imageLiteral(resourceName: "isFavorite"), for: UIControlState.normal)
            cell.game = game
        }
        
        cell.removeButton.tag = indexPath.row
        cell.gerirJogosViewController = self
        return cell
    }
    
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(segmentedStatus == 0 && myGames != nil){
            var gameId:Int = myGames[indexPath.row]["id"] as! Int
            if(gameId != nil){
                apiIgbd.getGameInfo(gameId, completion: { ( game, error) in
                    if(game != nil){
                        var game = game
                        TwitchApi.getStreamsByGameName(filter: (game?.name!)!, completion: { (channels, error) in
                            game?.twitchChannels = channels
                            if(game?.coverUrl == nil || game?.coverUrl == "N/A"){
                                DispatchQueue.main.async {
                                    self.tableView.reloadData()
                                    self.performSegue(withIdentifier: "mySegue", sender: game)
                                }
                            }else{
                                Game.getGameImage((game?.coverUrl!)!, completion: { (gameImage, error) in
                                    game?.coverImage = gameImage
                                    DispatchQueue.main.async {
                                        self.tableView.reloadData()
                                        self.performSegue(withIdentifier: "mySegue", sender: game)
                                    }
                                })
                            }
                        })
                    }
                })
            }
           // print("Game \(myGames[indexPath.row].name)")
        }
        
        if(segmentedStatus == 1 && favorites != nil){
            var gameId:Int = favorites[indexPath.row]["id"] as! Int
            if(gameId != nil){
                apiIgbd.getGameInfo(gameId, completion: { ( game, error) in
                    if(game != nil){
                        var game = game
                        TwitchApi.getStreamsByGameName(filter: (game?.name!)!, completion: { (channels, error) in
                            game?.twitchChannels = channels
                            if(game?.coverUrl == nil || game?.coverUrl == "N/A"){
                                DispatchQueue.main.async {
                                    self.tableView.reloadData()
                                    self.performSegue(withIdentifier: "mySegue", sender: game)
                                }
                            }else{
                                Game.getGameImage((game?.coverUrl!)!, completion: { (gameImage, error) in
                                    game?.coverImage = gameImage
                                    DispatchQueue.main.async {
                                        self.tableView.reloadData()
                                        self.performSegue(withIdentifier: "mySegue", sender: game)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        }
        
        
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScene = segue.destination as? GameInfoViewController {
            nextScene.game =  sender as! Game
            
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
