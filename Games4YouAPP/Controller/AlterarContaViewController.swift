//
//  AlterarContaViewController.swift
//  Games4YouAPP
//
//  Created by formando on 28/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit
import MobileCoreServices
import QuartzCore

class AlterarContaViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var textPrimeiroNome: UITextField!
    @IBOutlet weak var textUltimoNome: UITextField!
    @IBOutlet weak var textNewPassword: UITextField!
    @IBOutlet weak var textConfirmNewPassword: UITextField!
    @IBOutlet weak var textErorr: UILabel!
    
    @IBOutlet weak var labelConfirmPassword: UILabel!
    @IBOutlet weak var labelNewPassword: UILabel!
    @IBOutlet weak var textSteam: UITextField!
    
    @IBOutlet weak var textOrigin: UITextField!
    var user = Repositorio.repositorio.users[0]
    var aux = #imageLiteral(resourceName: "icons8-customer-filled-100")
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        var user = Repositorio.repositorio.users[0]
       
        super.viewDidLoad()
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tap)
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        if user.userid != nil {
            textNewPassword.isHidden = true
            textConfirmNewPassword.isHidden = true
            labelNewPassword.isHidden = true
            labelConfirmPassword.isHidden = true
        }
        
        if user.givenName != nil {
            textPrimeiroNome.text = user.givenName
        }
        if user.familyName != nil {
            textUltimoNome.text = user.familyName
        }
        
        if user.origin != nil{
            textOrigin.text = user.origin
        }
        if user.steam != nil {
            textSteam.text = user.steam
        }
        if user.image != nil {
        imageView.image = user.imageView
          /*  DispatchQueue.main.async {
                User.getGameImage(user.image!, completion: { (imageUser, error) in
                    self.imageView.image = imageUser
                })
            }*/
        }
        // textNewPassword.text = user.password
        // textConfirmNewPassword.text = user.password
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    @IBAction func takePicture(_ sender: Any) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            print("This device doesn't have a camera.")
            return
        }
        
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .front
        //        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for:.camera)!
        imagePicker.delegate = self
        
        present(imagePicker, animated: true)
    }
    

    
    @objc func dismissKeyboard(_ sender:UIButton!){
           view.endEditing(true)
    }
    @IBAction func alterarUtilizador(_ sender: UIButton) {
       
        Repositorio.repositorio.removeUser()
        var primeiroNome = ""
        var ultimoNome = ""
        var newPassword = ""
        var origin = ""
        var steam = ""

        if textPrimeiroNome.text != nil {
            primeiroNome = textPrimeiroNome.text!
        }
        if textUltimoNome.text != nil {
            ultimoNome = textUltimoNome.text!
        }
        if textSteam.text != nil {
            steam = textSteam.text!
        }
        if textOrigin.text != nil {
            origin = textOrigin.text!
        }
        
        if (textNewPassword.text != nil && textNewPassword.text != "") {
            if textNewPassword.text == textConfirmNewPassword.text {
                newPassword = textNewPassword.text!
                textErorr.text = ""
            } else {
                textErorr.text = "As password estao diferentes"
                return
            }
        } else {
            if user.userid == nil {
                newPassword = user.password!
            }

        }
        user.origin = origin
        user.steam = steam
        user.givenName = primeiroNome
        user.familyName = ultimoNome
        user.password = newPassword
        Repositorio.repositorio.guardarUser()
       // Repositorio.repositorio.users.first?.givenName = primeiroNome
        
        
        Autentication.alterar(user: user, completion: { (user, error) in
            } )
            let alert = UIAlertController(title: "Games4You", message: "Alterou a conta", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
            { action in
                    self.performSegue(withIdentifier: "alterouSegue", sender: nil)
            }
        ))
        
        self.present(alert, animated: true, completion: nil)

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination as? PesquisaJogosViewController
        segue.destination.navigationItem.hidesBackButton = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
    //    imageView.layer.cornerRadius = imageView.frame.size.width/2
  //      imageView.layer.masksToBounds = true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            picker.dismiss(animated: true)
        }
        
        print(info)
        // get the image
        guard var image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        // do something with it
        image = image.fixOrientation()
        imageView.image = image
        var random = String(user.id!)
        random += randomString(len: 10)
        
        self.user.imageView = image
        Repositorio.repositorio.users[0] = self.user
        Repositorio.repositorio.guardarUser()
        
        CloudinaryHelper.sharedInstance.uploadImage(image: image , name_image: random,  completion: { (status, url) in
            DispatchQueue.main.async {
                self.user.image = url
                Autentication.alterar(user: self.user, completion: { (user, error) in
                   
                } )
            }
        })
        
        // guardar no repositório e guardar no servidor
        
        
    }
    func randomString(len:Int) -> String {
        let charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var c = Array(charSet)
        var s:String = ""
        for n in (1...10) {
            s.append(c[Int(arc4random()) % c.count])
        }
        return s
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        defer {
            picker.dismiss(animated: true)
        }
        
        print("did cancel")
    }
    
    
}
extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImageOrientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}
