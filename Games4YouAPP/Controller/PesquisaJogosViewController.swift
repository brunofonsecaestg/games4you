//
//  PesquisaJogosViewController.swift
//  Games4YouAPP
//
//  Created by José Pedro Nascimento Guerra on 27/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class PesquisaJogosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
  
    var games:[Game]! = []
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet var TbData: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var navBar: UINavigationItem!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
         Repositorio.repositorio.obterUser()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        TbData.delegate = self
        TbData.dataSource = self
        searchBar.delegate = self

        if NetworkConnection.isConnectedToInternet() == false {
            Repositorio.repositorio.obterJogo()
            appDelegate.games = Repositorio.repositorio.jogos
            games = Repositorio.repositorio.jogos
            TbData.reloadData()
            return
        }
        
        if(appDelegate.topGames.count > 0){
            games = appDelegate.topGames
            navBar.title = "Top Games"
            TbData.reloadData()
            return
        }
       
            apiIgbd.getTop( completion: {(games, error) in
                Repositorio.repositorio.jogos.removeAll()
                
                self.appDelegate.games.removeAll()
                self.games.removeAll()
                
                for var game in games! {
                    TwitchApi.getStreamsByGameName(filter: game.name!, completion: { (channels, error) in
                        game.twitchChannels = channels
                        Game.getGameImage(game.coverUrl!, completion: { (gameImage, error) in
                            game.coverImage = gameImage
                            self.appDelegate.games.append(game)
                            self.appDelegate.topGames.append(game)
                            
                            Repositorio.repositorio.jogos.append(game)
                           
                            DispatchQueue.main.async {
                                Repositorio.repositorio.guardarJogo()
                                self.games = self.appDelegate.topGames
                                self.TbData.reloadData()
                            }
                        })
                        
                    })
  
                }
            })

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if NetworkConnection.isConnectedToInternet() == false {
            let alert = UIAlertController(title: "Games4You", message: "Tem de estar ligado a Internet!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.cancel, handler:   { action in
            }))
            
            self.present(alert, animated: true, completion: nil)
           return
        }
        
        if searchBar.text != ""{
            navBar.title = "Pesquisa de Jogos"
            apiIgbd.getSearchedGames(searchBar.text!, completion: { (searchGamesIds, error) in
                self.games = []
               // Repositorio.repositorio.jogos.removeAll()
                DispatchQueue.main.async {
                    self.TbData.reloadData()
                }
                
                if searchGamesIds != nil {
                    self.appDelegate.games = []
                    for gameId in searchGamesIds!{
                        apiIgbd.getGameInfo(gameId, completion: { ( game, error) in
                            if(game != nil){
                                var game = game
                                TwitchApi.getStreamsByGameName(filter: (game?.name!)!, completion: { (channels, error) in
                                    game?.twitchChannels = channels
                                    if(game?.coverUrl == nil || game?.coverUrl == "N/A"){
                                        self.games.append(game!)
                                          self.appDelegate.games.append(game!)
                                      //  Repositorio.repositorio.jogos.append(game!)
                                        DispatchQueue.main.async {
                                            self.TbData.reloadData()
                                        //    Repositorio.repositorio.guardarJogo()
                                        }
                                    }else{
                                        Game.getGameImage((game?.coverUrl!)!, completion: { (gameImage, error) in
                                            game?.coverImage = gameImage
                                            self.games.append(game!)
                                              self.appDelegate.games.append(game!)
                                          //  Repositorio.repositorio.jogos.append(game!)
                                            DispatchQueue.main.async {
                                                self.TbData.reloadData()
                                            //    Repositorio.repositorio.guardarJogo()
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }else {
            navBar.title = "Top Games"
            games = Repositorio.repositorio.jogos
            TbData.reloadData()
        }
        searchBar.endEditing(true)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setValue("Cancelar", forKey: "_cancelButtonText")
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        
        // Remove focus from the search bar.
        searchBar.endEditing(true)
        
        if(appDelegate.topGames.count > 0){
            games = appDelegate.topGames
            navBar.title = "Top Games"
            TbData.reloadData()
            return
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return games.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameCell", for: indexPath) as! GameTableViewCell
    
        if games.count >= indexPath.row+1
        {
            let game = games[indexPath.row]
            cell.game = game
        }
        
        return cell
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       performSegue(withIdentifier: "mySegue", sender: games[indexPath.row])
        TbData.deselectRow(at: indexPath, animated: true)
      //  print("Game \(games[indexPath.row].name)")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScene = segue.destination as? GameInfoViewController {
            nextScene.game =  sender as! Game
        }
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
