//
//  VideoCollectionViewCell.swift
//  Games4YouAPP
//
//  Created by formando on 15/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VideoCollectionViewCell: UICollectionViewCell {
    
  
    @IBOutlet weak var videoPlayer: YTPlayerView!
    
}
