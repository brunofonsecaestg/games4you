//
//  GameWithRemoveViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 05/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class GameWithRemoveViewCell: UITableViewCell {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet var gameImage: UIImageView!
    @IBOutlet var removeButton: UIButton!
    @IBOutlet var nameLabel: UILabel!
    
    var gerirJogosViewController: GerirJogosViewController?
    
    var mode = ""
    
    var game:[String:Any]?{
        didSet{
            nameLabel.text = self.game?["name"] as! String
            
            if(self.game?["coverImage"] == nil){
                self.game?["coverImage"] = #imageLiteral(resourceName: "gameImageDefault")
            }
            
            gameImage.image = self.game?["coverImage"] as! UIImage
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    @IBAction func removeButtonAction(_ sender: Any) {
        print(removeButton.tag)
        //print(status)
        var status = Autentication.getMyGamesAndFavoriteStatus(param: self.mode, gameId: self.game?["id"] as! Int)
        print("status \(status)")
        
        let gameIdAux: Int = self.game?["id"] as! Int
        let gameId = String(describing:gameIdAux)
        
        if(self.mode == "myGames"){
            let alert = UIAlertController(title: "Games4You", message: "Tem a certeza que pretende efectuar esta operação?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancelar",style: UIAlertActionStyle.cancel, handler:   { action in
                return
            }))
            alert.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.default, handler:
                { action in
                    if(status == true){
                        // enviar pedido com o meujogo a 0 e o favorito a 1
                        Game.alterGame(idUtilizador: String(describing: Repositorio.repositorio.users[0].id!) , idJogo:  gameId, meuJogo: "0", favorito: "1", name: self.game?["name"]! as! String,coverUrl: self.game?["cover"]! as! String) { ( error) in
                            print("feito")
                            DispatchQueue.main.async {
                                self.appDelegate.myGames.remove(at: self.removeButton.tag)
                                self.gerirJogosViewController?.updateMyGamesAndFavorites()
                            }
                        }
                    }else{
                        // enviar pedido com o meujogo a 0 e o favorito a 0
                        Game.alterGame(idUtilizador: String(describing: Repositorio.repositorio.users[0].id!) , idJogo:  gameId, meuJogo: "0", favorito: "0", name: self.game?["name"]! as! String,coverUrl: self.game?["cover"]! as! String) { ( error) in
                            print("feito")
                            DispatchQueue.main.async {
                                self.appDelegate.myGames.remove(at: self.removeButton.tag)
                                self.gerirJogosViewController?.updateMyGamesAndFavorites()
                            }
                        }
                    }
                }
            ))
            gerirJogosViewController?.present(alert, animated: true, completion: nil)
        }
        
        if(self.mode == "favorites"){
            let alert = UIAlertController(title: "Games4You", message: "Tem a certeza que pretende efectuar esta operação?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancelar",style: UIAlertActionStyle.cancel, handler:   { action in
                return
            }))
            alert.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.default, handler:
                { action in
                    if(status == true){
                        // enviar pedido com o favorite a 0 e o meujogo a 1
                        Game.alterGame(idUtilizador: String(describing: Repositorio.repositorio.users[0].id!) , idJogo:  gameId, meuJogo: "1", favorito: "0", name: self.game?["name"]! as! String,coverUrl: self.game?["cover"]! as! String) { ( error) in
                            print("feito")
                            DispatchQueue.main.async {
                                self.appDelegate.favorites.remove(at: self.removeButton.tag)
                                self.gerirJogosViewController?.updateMyGamesAndFavorites()
                            }
                        }
                    }else{
                        // enviar pedido com o favorite a 0 e o meujogo a 0
                        Game.alterGame(idUtilizador: String(describing: Repositorio.repositorio.users[0].id!) , idJogo:  gameId, meuJogo: "0", favorito: "0", name: self.game?["name"]! as! String,coverUrl: self.game?["cover"]! as! String) { ( error) in
                            print("feito")
                            DispatchQueue.main.async {
                                self.appDelegate.favorites.remove(at: self.removeButton.tag)
                                self.gerirJogosViewController?.updateMyGamesAndFavorites()
                            }
                        }
                    }
                }
            ))
            gerirJogosViewController?.present(alert, animated: true, completion: nil)
          
        }
    }
}
