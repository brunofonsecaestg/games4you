//
//  GameTableViewCell.swift
//  Games4YouAPP
//
//  Created by Admin on 26/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var ratingValue: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var twitchLabel: UILabel!
    @IBOutlet weak var youtubeLabel: UILabel!
    var game:Game?{
        didSet{
            nameLabel.text = self.game?.name
            youtubeLabel.text = String(describing: self.game!.videos!.count)
            twitchLabel.text = String(describing:self.game!.twitchChannels!.count)
            
            if self.game?.coverImage != nil {
                gameImage.image = self.game?.coverImage
            } else{
                gameImage.image = #imageLiteral(resourceName: "gameImageDefault")
            }
            
            ratingValue.text = "\(game!.rating!.rounded())"
            if (self.game?.rating!)! == 0 {
                ratingImage.image = #imageLiteral(resourceName: "0")
                return
            }
            if (self.game?.rating!)! >= 75.0 {
                ratingImage.image = #imageLiteral(resourceName: "4")
                 return
            }
            if (self.game?.rating!)! < 75.0  && (self.game?.rating!)! >= 50.0 {
                ratingImage.image = #imageLiteral(resourceName: "3")
                 return
            }
            if (self.game?.rating!)! < 50.0  && (self.game?.rating!)! >= 25.0 {
                ratingImage.image = #imageLiteral(resourceName: "2")
                 return
            }
            if (self.game?.rating!)! < 25.0 {
                ratingImage.image = #imageLiteral(resourceName: "1")
            }
          
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
