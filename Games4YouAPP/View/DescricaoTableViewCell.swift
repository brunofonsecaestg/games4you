//
//  DescricaoTableViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno José Fonseca on 13/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class DescricaoTableViewCell: UITableViewCell {

    @IBOutlet weak var descricao: UILabel!
    var gameViewController : GameInfoViewController?
    
    @IBOutlet weak var comentarioText: UITextField!
    var game:Game?{
        didSet{
            
            descricao.text = game!.desc
            descricao.sizeToFit()
            descricao.lineBreakMode = .byWordWrapping;
        }
    }
    
    var user:User?{
        didSet{
            
         
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @objc func donePressed(_ sender:UIButton!){
        if NetworkConnection.isConnectedToInternet() == false {
            gameViewController?.view.endEditing(true)
            let alert = UIAlertController(title: "Games4You", message: "Tem de estar ligado a Internet!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.cancel, handler:   { action in
            }))
          
            gameViewController?.present(alert, animated: true, completion: nil)
            
            return
        }
        if (comentarioText.text != nil){
            if user == nil {
                let alert = UIAlertController(title: "Games4You", message: "Tem de estar logado!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancelar",style: UIAlertActionStyle.cancel, handler:   { action in
                }))
                alert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler:
                    { action in
                        self.gameViewController?.performSegue(withIdentifier: "LoginSegue", sender: nil)
                }
                ))
                gameViewController?.present(alert, animated: true, completion: nil)
                return
            }
            
            var comentario = comentarioText.text
            comentarioText.text = ""
            Game.addComentario(texto: comentario!, idJogo: String(describing: game!.id!), idUtilizador: String(describing: user!.id!),gameName: (game?.name!)!, completion: { (id,error) in
                var c = Comentario(id: id, text: comentario!, idUser: (self.user?.id!)!,emailUser: (self.user?.email!)!)
                self.game?.comentarios?.append(c)
                self.gameViewController?.tbData.reloadData()
            })
            
        }
        gameViewController?.view.endEditing(true)
    }
    
    @objc func cancelPressed(_ sender:UIButton!){
        gameViewController?.view.endEditing(true)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func addToolBar(textField: UITextField){
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        
        var doneButton = UIBarButtonItem(title: "Enviar", style:  UIBarButtonItemStyle.done, target: self, action: #selector(self.donePressed(_:)))
        var cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self,action: #selector(self.cancelPressed(_:)))
        var spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
       //textField.delegate = self as! UITextFieldDelegate
        textField.inputAccessoryView = toolBar
    }

}
