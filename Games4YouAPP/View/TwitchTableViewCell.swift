//
//  TwitchTableViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno José Fonseca on 13/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class TwitchTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if NetworkConnection.isConnectedToInternet() == true {
            
            return game!.twitchChannels!.count
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "twitchVideoCell", for: indexPath) as! TwitchCollectionViewCell
        var channel = game?.twitchChannels![indexPath.row]
        
        cell.twitchPlayer.scrollView.isScrollEnabled = false
        
        var url = "<iframe src=\"https://player.twitch.tv/?channel="+channel!+"&autoplay=false\" frameborder=\"0\" allowfullscreen=\"true\" scrolling=\"yes\" width=\"50%\" height=\"100%\" ></iframe>"
        print(url)
        
        cell.twitchPlayer.loadHTMLString(url, baseURL: nil)
        
        return cell
    }
    
    @IBOutlet weak var TwitchCollectionView: UICollectionView!
    
    @IBOutlet weak var textNoStream: UILabel!
    
    var game:Game?{
        didSet{
            
            if game?.twitchChannels?.count == 0 {
                textNoStream.isHidden = false
            }
            if NetworkConnection.isConnectedToInternet() == false {
                textNoStream.text = "No Internet Connection"
                textNoStream.isHidden = false
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
