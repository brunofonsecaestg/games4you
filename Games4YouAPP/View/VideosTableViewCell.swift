//
//  VideosTableViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno José Fonseca on 13/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class VideosTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let playersVars = ["origin":"https://www.youtube.com"]
      
    @IBOutlet weak var VideoCollectionView: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if NetworkConnection.isConnectedToInternet() == true {
            return (game!.videos?.count)!
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gameVideoCell", for: indexPath) as! VideoCollectionViewCell
        cell.videoPlayer.load(withVideoId: (game?.videos![indexPath.row])!, playerVars: playersVars)
        return cell
    }
    

    @IBOutlet weak var videosCollection: UICollectionView!
    
    @IBOutlet weak var textNoVideos: UILabel!
    var game:Game?{
        didSet{
            if game?.videos?.count == 0 {
                textNoVideos.isHidden = false
            }
            if NetworkConnection.isConnectedToInternet() == false {
                textNoVideos.text = "No Internet Connection"
                textNoVideos.isHidden = false
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
