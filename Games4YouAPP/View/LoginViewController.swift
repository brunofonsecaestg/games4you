//
//  LoginViewController.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 31/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn



class LoginViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var btnMenuButton: UIBarButtonItem!
    @IBOutlet weak var textUsername: UITextField!
    
    @IBOutlet weak var signIn: UIButton!
    @IBOutlet weak var textPassword: UITextField!
    
    @IBOutlet weak var googleSignButton: GIDSignInButton!
    
    
    @IBAction func singInButton(_ sender: UIButton) {
        Autentication.login(username: textUsername.text!, password: textPassword.text!, completion: { (user, error) in
            if user != nil {
                DispatchQueue.main.sync {
                    let alert = UIAlertController(title: "Games4You", message: "\(self.textUsername.text!), esta logado!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                        { action in
                            self.performSegue(withIdentifier: "LoginSegue", sender: nil)
                        }
                    ))
                    self.present(alert, animated: true, completion: nil)
                    if user?.image == nil || user?.image == ""{
                        user?.imageView = #imageLiteral(resourceName: "icons8-customer-filled-100")
                        Repositorio.repositorio.users.append(user!)
                        Repositorio.repositorio.guardarUser()
                    }else {
                        User.getGameImage((user?.image)!, completion: { (image, error) in
                            user?.imageView = image
                            Repositorio.repositorio.users.append(user!)
                            Repositorio.repositorio.guardarUser()
                        })
                    }
                }
                
               
                //  self.performSegue(withIdentifier: "mySegue", sender: nil)
            } else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Games4You", message: "Username e Password estão incorretos", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
    }
    @objc func dismissKeyboard(_ sender:UIButton!){
        
      
        view.endEditing(true)
        
    }
    
    func dismissOnTap(){
        self.view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target:self, action:#selector(LoginViewController.dismissKeyboard(_:)))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view is GIDSignInButton {
            return false
        }
        return true
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var error : NSError?
        self.navigationItem.hidesBackButton = true
        
       dismissOnTap()
        
        if(Repositorio.repositorio.users.count == 0)
        {
            GGLContext.sharedInstance().configureWithError(&error)
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
            //let googleSignButton = GIDSignInButton()
            
            googleSignButton.style = GIDSignInButtonStyle.wide
            //googleSignButton.contentVerticalAlignment = .center
        
           //  googleSignButton.center = view.center
            //view.addSubview(googleSignButton)
            
        }
       //let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(LoginViewController.dismissKeyboard(_:)))
      //  self.view.addGestureRecognizer(tap)
        if self.revealViewController() != nil
        {
            btnMenuButton.target = self.revealViewController()
            btnMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
           // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationItem.hidesBackButton = true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if(error == nil){
        
            let userid = user.userID
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
     /// Metodo Webservice
            var user = User(id: nil, userid: userid!,  givenName: givenName!, familyName: familyName!, email: email!,password: nil,steam:nil, origin:nil,image:nil,imageView:nil)
            Autentication.loginGoogle(user: user, completion: { (user, error) in
           
                if user?.image == nil || user?.image == ""{
                    user?.imageView = #imageLiteral(resourceName: "icons8-customer-filled-100")
                    Repositorio.repositorio.users.append(user!)
                    Repositorio.repositorio.guardarUser()
                }else {
                    User.getGameImage((user?.image)!, completion: { (image, error) in
                        user?.imageView = image
                        Repositorio.repositorio.users.append(user!)
                        Repositorio.repositorio.guardarUser()
                    })
                }
                
            } )

            performSegue(withIdentifier: "LoginSegue", sender: nil)
      

        }else{
            print("\(error.localizedDescription)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination as? PesquisaJogosViewController
        segue.destination.navigationItem.hidesBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
