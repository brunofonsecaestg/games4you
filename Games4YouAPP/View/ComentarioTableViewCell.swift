//
//  ComentarioTableViewCell.swift
//  
//
//  Created by Bruno José Fonseca on 06/12/2017.
//

import UIKit

class ComentarioTableViewCell: UITableViewCell {

    
    @IBOutlet weak var comentarioText: UILabel!
    @IBOutlet weak var utilizadorText: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    var gameViewController : GameInfoViewController?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var comentario:Comentario?{
        didSet{
            
            comentarioText.text = comentario?.text
            comentarioText.lineBreakMode = .byWordWrapping;
            utilizadorText.text = comentario?.emailUser
            if Repositorio.repositorio.users.count != 0 {
                if comentario?.idUser != Repositorio.repositorio.users[0].id {
                    removeButton.isHidden = true        
                }else{
                    removeButton.isHidden = false
                }
            }else{
                removeButton.isHidden = true
            }
        }
    }
    @IBAction func apagarComentario(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Games4You", message: "Tem a certeza que pretende remover o comentário?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancelar",style: UIAlertActionStyle.cancel, handler:   { action in
            return
        }))
        alert.addAction(UIAlertAction(title: "Sim",style: UIAlertActionStyle.default, handler:   { action in
            Game.removeComentario(id: String(describing: self.comentario!.id!)) { (error) in
                self.gameViewController?.removeComentario()
            }
        }))
        self.gameViewController?.present(alert, animated: true, completion: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
