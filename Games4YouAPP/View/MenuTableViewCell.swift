//
//  MenuTableViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 31/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMenuName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
