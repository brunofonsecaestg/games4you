//
//  GameInfoTableViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno José Fonseca on 13/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit

class GameInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var favoritesButton: UIButton!
    @IBOutlet weak var myGamesButton: UIButton!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var gameImage: UIImageView!
    
    var game:Game?{
        didSet{
            if game?.coverImage != nil {
                
                gameImage.image = game?.coverImage
                    } else{
                        gameImage.image = #imageLiteral(resourceName: "gameImageDefault")
                    }
        
        
            if game?.rating != nil {
                ratingLabel.text = "\(game!.rating!.rounded())"
                    }else {
                        ratingLabel.text = "N/A"
                    }
            
                dataLabel.text = game!.first_release_date
                dataLabel.sizeToFit()
        }
    }
    
    var user:User?{
        didSet{
            
            
        }
    }
       var gameViewController : GameInfoViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func alterMyGame(_ sender: Any) {
        var aux:String
        var auxFavorito:String
        
        if user == nil {
            let alert = UIAlertController(title: "Games4You", message: "Tem de estar logado!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancelar",style: UIAlertActionStyle.cancel, handler:   { action in
            }))
            alert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler:
                { action in
                    self.gameViewController?.performSegue(withIdentifier: "LoginSegue", sender: nil)
            }
            ))
            gameViewController?.present(alert, animated: true, completion: nil)
            return
        }
        if NetworkConnection.isConnectedToInternet() == false {
            gameViewController?.view.endEditing(true)
            let alert = UIAlertController(title: "Games4You", message: "Tem de estar ligado a Internet!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.cancel, handler:   { action in
            }))
            
            gameViewController?.present(alert, animated: true, completion: nil)
            return
        }
        
        if myGamesButton.currentImage == #imageLiteral(resourceName: "add"){
            aux = "1"
            myGamesButton.setImage(#imageLiteral(resourceName: "remove"), for: .normal)
        }else{
            aux = "0"
            myGamesButton.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        }
        if favoritesButton.currentImage == #imageLiteral(resourceName: "isFavorite"){
            auxFavorito = "1"
        }else{
            auxFavorito = "0"
        }
        
        Game.alterGame(idUtilizador: String(describing: user!.id!) , idJogo: String(describing: game!.id!) , meuJogo: aux, favorito: auxFavorito,  name: (game!.name!),coverUrl: (game!.coverUrl!))
        { ( error) in     }
        
    }
    
    
    @IBAction func alterFavoriteGame(_ sender: Any) {
        var auxFavorito:String
        var aux:String
        
        if user == nil {
            let alert = UIAlertController(title: "Games4You", message: "Tem de estar logado!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancelar",style: UIAlertActionStyle.cancel, handler:   { action in
            }))
            alert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler:
                { action in
                    self.gameViewController?.performSegue(withIdentifier: "LoginSegue", sender: nil)
            }
            ))
            gameViewController?.present(alert, animated: true, completion: nil)
            return
        }
        if NetworkConnection.isConnectedToInternet() == false {
          
            let alert = UIAlertController(title: "Games4You", message: "Tem de estar ligado a Internet!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK",style: UIAlertActionStyle.cancel, handler:   { action in
            }))
            
            gameViewController?.present(alert, animated: true, completion: nil)
            return
        }
        if myGamesButton.currentImage == #imageLiteral(resourceName: "add"){
            aux = "0"
        }else{
            aux = "1"
        }
        if favoritesButton.currentImage == #imageLiteral(resourceName: "isFavorite"){
            favoritesButton.setImage(#imageLiteral(resourceName: "notFavorite") ,for: .normal)
            auxFavorito = "0"
        }else{
            auxFavorito = "1"
            favoritesButton.setImage(#imageLiteral(resourceName: "isFavorite"), for: .normal)
        }
        
        Game.alterGame(idUtilizador: String(describing: user!.id!) , idJogo: String(describing: game!.id!) , meuJogo: aux, favorito: auxFavorito, name: (game!.name!),coverUrl: (game!.coverUrl!)) { ( error) in }
      
    }
    
}
