//
//  TwitchCollectionViewCell.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 16/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation
import WebKit

class TwitchCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var twitchPlayer: WKWebView!
        
    
}
