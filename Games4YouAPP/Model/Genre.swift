//
//  Genre.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 08/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation

struct Genre {
    var id:Int?
    var name:String?

    static func parseJson(json:[String:Any]) -> Genre? {

        guard let id = json["id"] as? Int else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        return Genre(id: id, name: name)
    }
    
}
