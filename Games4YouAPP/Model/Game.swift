//
//  Game.swift
//  Games4YouAPP
//
//  Created by Admin on 26/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//
import UIKit
import Foundation

class Game: NSObject, NSCoding {
    var id:Int?
    var name:String?
    var desc:String?
    var rating:Double?
    var popularity:Double?
    var first_release_date:String?
    var genres:[Genre]? = []
    var videos:[String]? = []
    var coverUrl:String?
    var coverImage:UIImage?
    var twitchChannels:[String]?
    var comentarios:[Comentario]?
    
    init(id: Int, name: String, desc: String,rating: Double, popularity: Double, first_release_date: String, genres: [Genre], videos: [String], coverUrl:String, coverImage: UIImage?, twitchChannels: [String]?,comentarios: [Comentario]?) {
        self.id = id
        self.name = name
        self.desc = desc
        self.rating = rating
        self.popularity = popularity
        self.first_release_date = first_release_date
        self.genres = genres
        self.videos = videos
        self.coverUrl = coverUrl
        self.coverImage = coverImage
        self.twitchChannels = twitchChannels
        self.comentarios = comentarios
        

    }
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(popularity, forKey: "popularity")
        aCoder.encode(first_release_date, forKey: "first_release_date")
      //aCoder.encode(genres, forKey: "genres")
        aCoder.encode(videos, forKey: "videos")
        aCoder.encode(coverUrl, forKey: "coverUrl")
        aCoder.encode(coverImage, forKey: "coverImage")
        aCoder.encode(twitchChannels, forKey: "twitchChannels")
      //  aCoder.encode(comentarios, forKey: "comentarios")
       
        
    }
    
    required init?(coder: NSCoder) {
        id = coder.decodeObject(forKey:"id") as? Int
        name = coder.decodeObject(forKey:"name") as? String
        desc = coder.decodeObject(forKey:"desc") as? String
        rating = coder.decodeObject(forKey: "rating") as? Double
        popularity = coder.decodeObject(forKey: "popularity") as? Double
        first_release_date = coder.decodeObject(forKey: "first_release_date") as? String
      // genres = coder.decodeObject(forKey: "genres") as? [Genre]
        videos = coder.decodeObject(forKey: "videos") as? [String]
        coverUrl = coder.decodeObject(forKey: "cover_url") as? String
        coverImage = coder.decodeObject(forKey: "coverImage") as? UIImage
        twitchChannels = coder.decodeObject(forKey: "twitchChannels") as? [String]
        comentarios = coder.decodeObject(forKey: "comentarios") as? [Comentario]
        
    }
    
    static func parseJson(json:[String:Any]) -> Game? {
        let desc:String?
        let rating:Double?
        let popularity:Double?
        let first_release_date:Double?
        var date:String?
        var dateAux:Date?
        var genres:[Genre] = []
        var videos:[String] = []
        var coverUrl:String?
        var image:UIImage?
        
        guard let id = json["id"] as? Int else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        if (json["summary"] != nil){
            desc = json["summary"] as? String
        } else {
            desc = "N/A"
        }
       
        if (json["rating"] != nil){
            rating = json["rating"] as? Double
        } else {
            rating = 0.0
        }
        
        if (json["popularity"] != nil){
            popularity = json["popularity"] as? Double
        } else {
            popularity = nil
        }
        
        if (json["first_release_date"] != nil){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            first_release_date = json["first_release_date"] as? Double
            dateAux = Date(timeIntervalSince1970: (first_release_date! / 1000.0))
            date = dateFormatter.string(from: dateAux!)
        } else {
            date = "N/A"
        }
        
        if (json["genres"] != nil){
            let genresJson = json["genres"] as? [[String:Any]]
            if(genresJson != nil ){
                for item in genresJson! {
                    let genre = Genre.parseJson(json: item)
                    genres.append(genre!)
                }
            }else  {
                genres = []
            }
        }
        
        if (json["videos"] != nil){
            let videosJson = json["videos"] as? [[String:Any]]
            if(videosJson != nil ){
                for item in videosJson! {
                    let video_id = item["video_id"] as? String
                    videos.append(video_id!)
                }
            }else  {
                videos = []
            }
        }
        
        if (json["cover"] != nil){
            let coverJson = json["cover"] as? [String: Any]
            coverUrl = (coverJson?["url"] as! String)
            if(coverUrl?.starts(with: "//"))!{
                coverUrl = "https:" + coverUrl!
                
            }
        } else  {
            
            coverUrl = "N/A"
        }
        

        return Game(id: id, name: name, desc: desc!,rating: rating!, popularity: popularity!, first_release_date: date!, genres: genres, videos: videos, coverUrl:coverUrl!, coverImage:nil, twitchChannels: nil,comentarios:nil)
    }
    
    
    public static func getGameImage(_ urlImage:String, completion: @escaping (_ image: UIImage?, _ error:Error?) -> Void){
            
        let url = URL(string: urlImage)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                let image = #imageLiteral(resourceName: "gameImageDefault")
                
                completion(image, err)
                return
            }
            do {
                if let res = response as? HTTPURLResponse {
                    print("Downloaded picture with response code \(res.statusCode)")
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        //print(image)
                        completion(image, err)
                    } else {
                        print("Couldn't get image: Image is nil")
                        completion(nil, err)
                    }
                } else {
                    print("Couldn't get response code for some reason")
                    completion(nil, err)
                }
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    


    public static func alterGame( idUtilizador:String,idJogo: String,meuJogo: String,favorito:String, name:String,coverUrl:String,completion: @escaping (_ error:Error?) -> Void) {
            let idUtilizadorEnc = idUtilizador.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let idJogoEnc = idJogo.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let meuJogoEnc = meuJogo.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let favoritoEnc = favorito.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let nameEnc = name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let coverEnc = coverUrl.addingPercentEncoding(withAllowedCharacters: .urlPasswordAllowed)
        
        
            var urlString = Settings.Auth.baseUrlAuth+"AlterarEstadoJogo/idUtilizador="+idUtilizadorEnc!+"&idJogo="
            urlString += idJogoEnc!+"&meujogo="
            urlString += meuJogoEnc!+"&favorito="+favoritoEnc!+"&gameName="+nameEnc!+"&?cover="+coverEnc!
             //  /AlterarEstadoJogo/idUtilizador={idUtilizador}&idJogo={idJogo}&meujogo={meujogo}&favorito={favorito}

            let url = URL(string: urlString)
               var request = URLRequest(url: url!)
    
               request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
               request.httpMethod = "POST"
    
               let session = URLSession.shared
    
                let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
                if err != nil || data == nil {
                       completion( err)
                       return
                   }
                    do {
                       let httpresponse = response as! HTTPURLResponse
                       if(httpresponse.statusCode == 200)
                       {
    
                          let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
        
                        //  var user = User(id: jsonData["Id"] as? Int,userid: nil, givenName: jsonData["FirstName"] as? String, familyName: jsonData["LastName"] as? String, email: jsonData["Email"] as? String,password: password)
                           completion(err)
                       }else
                       {
                           completion(err)
                        }
                }
                catch {
                       completion(err)
                   }
               })
            dataTask.resume()
           }

    public static func getGameEstado( idUtilizador:String,idJogo: String,completion: @escaping (_ favorito:Int?,_ meuJogo:Int?,_ error:Error?) -> Void) {
            let idUtilizadorEnc = idUtilizador.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let idJogoEnc = idJogo.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
    
    
            var urlString = Settings.Auth.baseUrlAuth+"VerEstadoJogo/idUtilizador="+idUtilizadorEnc!+"&idJogo="+idJogoEnc!
    
        
            let url = URL(string: urlString)
               var request = URLRequest(url: url!)
    
               request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
               request.httpMethod = "GET"
    
               let session = URLSession.shared
    
               let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
                   if err != nil || data == nil {
                       completion( nil,nil,err)
                       return
                   }
                do {
                       let httpresponse = response as! HTTPURLResponse
                       if(httpresponse.statusCode == 200)
                    {
    
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                        var favorito = jsonData["favorito"] as? Int
                        var meuJogo = jsonData["meujogo"] as? Int
                        //  var user = User(id: jsonData["Id"] as? Int,userid: nil, givenName: jsonData["FirstName"] as? String, familyName: jsonData["LastName"] as? String, email: jsonData["Email"] as? String,password: password)
                            completion(favorito,meuJogo,err)
                        }else
                       {
                            completion(nil,nil,err)
                        }
                   }
                 catch {
                    completion(nil,nil,err)
                   }
            })
            dataTask.resume()
           }
    
    public static func getComentarios( idJogo: String,completion: @escaping (_ coms:[Comentario],_ error:Error?) -> Void) {
      
        
        
        var urlString = Settings.Auth.baseUrlAuth+"GetComentarios/idjogo="+idJogo
    
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        var comentarios:[Comentario] = []
        
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion( comentarios,err)
                return
            }
            do {
                let httpresponse = response as! HTTPURLResponse
                if(httpresponse.statusCode == 200)
                {
                    
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [[String:Any]]
                   
                    for item in jsonData {
                        
                        var texto = item["texto"] as! String
                        var email = item["email"] as! String
                        var id = item["id"] as! Int
                        var idUtilizador = item["idUtilizador"] as! Int
                        
                        var c = Comentario(id: id, text: texto, idUser: idUtilizador,emailUser: email)
                        comentarios.append(c)          
                    }
                    completion(comentarios,err)
                }else
                {
                    completion(comentarios,err)
                }
            }
            catch {
                completion(comentarios,err)
            }
        })
        dataTask.resume()
    }
    
    public static func addComentario( texto: String,idJogo: String,idUtilizador:String, gameName: String,completion: @escaping (_ id: Int,_ error:Error?) -> Void) {
        var id =  Int()
        
        let textoEnc = texto.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let gameNameEnc = gameName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        
        var urlString = Settings.Auth.baseUrlAuth+"AdicionarComentario/texto="+textoEnc!
        urlString += "&idjogo="+idJogo+"&idutilizador="+idUtilizador
        urlString += "&gameName="+gameNameEnc!
    
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
       
        
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(id,err)
                return
            }
            do {
                let httpresponse = response as! HTTPURLResponse
                if(httpresponse.statusCode == 200)
                    {
                    
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]

                         id = jsonData["id"] as! Int
       
                    completion(id,nil)
                }else
                {
                  completion(0,err)
                }
            }
            catch {
                completion(0,err)
            }
        })
        dataTask.resume()
    }
    
    public static func removeComentario( id: String,completion: @escaping (_ error:Error?) -> Void) {
       
        
    
        
        
        var urlString = Settings.Auth.baseUrlAuth+"RemoverComentario/id="+id
 
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "DELETE"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(err)
                return
            }
          
            do {
                let httpresponse = response as! HTTPURLResponse
                if(httpresponse.statusCode == 200)
                {
                    
                    completion(nil)
                }else
                {
                    completion(err)
                }
            }
            catch {
                completion(err)
            }
        })
        dataTask.resume()
    }
    
}

