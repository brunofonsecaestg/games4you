//
//  Autentication.swift
//  Games4YouAPP
//
//  Created by formando on 16/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation
class Autentication {
    
    
    
    public static func login( username: String,password: String,completion: @escaping (_ user: User?, _ error:Error?) -> Void) {
        let userEnc = username.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let passEnc = password.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let urlString = Settings.Auth.baseUrlAuth+"Login/userName="+userEnc!+"&password="+passEnc!
  
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)

        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let httpresponse = response as! HTTPURLResponse
                if(httpresponse.statusCode == 200)
                {
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                    var user = User(id: jsonData["Id"] as? Int,userid: nil, givenName: jsonData["FirstName"] as? String, familyName: jsonData["LastName"] as? String, email: jsonData["Email"] as? String,password: password,steam: jsonData["Steam"] as? String,origin: jsonData["Origin"] as? String,image: jsonData["Image"] as? String,imageView: nil)
                    completion(user,err)
                }else
                {
                    completion(nil,err)
                }
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    
    
    public static func loginGoogle( user: User,completion: @escaping (_ user: User?, _ error:Error?) -> Void) {
        var givenNameEnc=""
        var lastNameEnc=""
        var emailEnc = ""
        if (user.givenName != nil) {
            givenNameEnc = user.givenName!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if (user.familyName != nil) {
            lastNameEnc = user.familyName!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if (user.email != nil) {
            emailEnc = user.email!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
    
    
        let urlString = Settings.Auth.baseUrlAuth+"LoginGoogle/userId="+user.userid!+"&givenName="+givenNameEnc+"&lastName="+lastNameEnc+"&email="+emailEnc
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                var user = User(id: jsonData["Id"] as? Int, userid: user.userid, givenName: jsonData["FirstName"] as? String, familyName: jsonData["LastName"] as? String, email: jsonData["Email"] as? String,password: nil, steam: jsonData["Steam"] as? String,origin: jsonData["Origin"] as? String,image: jsonData["Image"] as? String ,imageView: nil)
                completion(user,err)
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    public static func register( username: String,password: String,email: String,completion: @escaping (_ user: User?, _ error:Error?) -> Void) {
        let userEnc = username.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let passEnc = password.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let emailEnc = email.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        var urlString = Settings.Auth.baseUrlAuth
        urlString += "RegistarUtilizador/userName=" + userEnc!
        urlString += "&password=" + passEnc!
        urlString += "&email=" + emailEnc!
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                if let actionString = jsonData["Error"] as? String
                {
                    let e = NSError(domain:jsonData["Error"] as! String, code: (response! as! HTTPURLResponse).statusCode, userInfo:nil)
                    completion(nil, e)
                    
                }
                else {
                    var user = User(id: jsonData["Id"] as? Int,userid: nil,  givenName: jsonData["FirstName"] as? String, familyName: jsonData["LastName"] as? String, email: jsonData["Email"] as! String,password: password,steam: nil,origin: nil,image:nil,imageView: nil)
                    completion(user,err)
                }
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    
    public static func alterar( user: User,completion: @escaping (_ user: User?, _ error:Error?) -> Void) {
        var givenNameEnc=""
        var lastNameEnc=""
        var emailEnc = ""
        var passwordEnc = "null"
        var idEnc = ""
        var steamEnc = ""
        var originEnc = ""
        var imageEnc = ""
        if (user.givenName != nil) {
            givenNameEnc = user.givenName!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if (user.familyName != nil) {
            lastNameEnc = user.familyName!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if (user.email != nil) {
            emailEnc = user.email!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if user.userid == nil {
             passwordEnc = user.password!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if user.steam != nil{
            steamEnc = user.steam!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if user.origin != nil {
            originEnc = user.origin!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        if user.image != nil {
            imageEnc = user.image!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        

        let idString:String! = String(describing: user.id!)
        var urlString = Settings.Auth.baseUrlAuth+"GuardarUtilizador/id=" + (idString) + ""
        urlString += "&?givenName="+givenNameEnc+"&?lastName="+lastNameEnc+"&?email="+emailEnc+"&?password="+passwordEnc
        urlString += "&?steam="+steamEnc+"&?origin="+originEnc+"&?image="+imageEnc
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        //&steam={STEAM}&origin={ORIGIN}&?image={IMAGE}
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "PUT"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                
                completion(nil,err)
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    
    public static func getMyGamesAndFavorites( user_id: String,completion: @escaping (_ myGames: [[String:Any]]?, _ favorites: [[String:Any]]?,_ error:Error?) -> Void) {
        
        let urlString = Settings.Auth.baseUrlAuth+"GetGamesByUser/idUtilizador="+user_id
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, nil,err)
                return
            }
            do {
                let httpresponse = response as! HTTPURLResponse
                if(httpresponse.statusCode == 200)
                {
                    let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [[String:Any]]
                    var myGames = [[String:Any]]()
                    var favorites = [[String:Any]]()
                    
                    for item in jsonData {
                        if (item["meujogo"] as! Bool == true) {
                            let game: [String: Any] = [
                                "id": item["id"] as Any,
                                "name": item["name"] as Any,
                                "cover": item["coverUrl"] as Any
                            ]
                            myGames.append(game)
                        }
                        if (item["favorito"] as! Bool == true) {
                            let game: [String: Any] = [
                                "id": item["id"] as Any,
                                "name": item["name"] as Any,
                                "cover": item["coverUrl"] as Any
                            ]
                            favorites.append(game)
                        }
                    }
                    
                    completion(myGames,favorites,err)
                }else
                {
                    completion(nil,nil,err)
                }
            }
            catch {
                completion(nil,nil,err)
            }
        })
        dataTask.resume()
    }
    
    public static func getMyGamesAndFavoriteStatus(param: String, gameId: Int) -> Bool{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(param == "myGames"){
            //pesquisar nos favoritos
            for game in appDelegate.favorites{
                var id:Int = game["id"] as! Int
                if(id == gameId){
                    return true
                }
            }
        }
        
        if(param == "favorites"){
            //pesquisar nos favoritos
            for game in appDelegate.myGames{
                var id:Int = game["id"] as! Int
                if(id == gameId){
                    return true
                }
            }
        }
        
        return false
    }

}
