//
//  CloudinaryHelper.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 11/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import UIKit
import Foundation
import Cloudinary

class CloudinaryHelper {
    static let sharedInstance = CloudinaryHelper()
    
    private let cloudinary_url = "cloudinary://631765984726144:6ggii2642T9E-8tGHc265J74SHw@games4you"
    
  
    func uploadImage(image: UIImage, name_image:String, completion: @escaping (_ status: Bool, _ url: String?) -> Void) {
        
        let config = CLDConfiguration(cloudName: "games4you", apiKey: "631765984726144:6ggii2642T9E-8tGHc265J74SHw")
        let cloudinary = CLDCloudinary(configuration: config)
        
        let forUpload = UIImagePNGRepresentation(image)
        let params = CLDUploadRequestParams()
        params.setPublicId(name_image)
        let uploader = cloudinary.createUploader()
        
        let request = uploader.upload(data: forUpload!, uploadPreset: "uinr5a0g", params: params, progress: { (progress) in
            //print(progress)
        }) { (result, error) in
            if(error == nil){
             //   print(result?.url)
                completion(true,result?.url)
            }else{
                 completion(false,nil)
            }
        }
        
        
    }
    
    

}
