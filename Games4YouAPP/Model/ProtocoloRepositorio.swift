//
//  ProtocoloRepositorio.swift
//  Games4YouAPP
//
//  Created by José Pedro Nascimento Guerra on 03/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation

import Foundation

protocol ProtocoloRepositorio{
    func obterJogo ()
    func guardarJogo()
    
    func obterUser ()
    func guardarUser()
}
