//
//  User.swift
//  Games4YouAPP
//
//  Created by José Pedro Nascimento Guerra on 02/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation


class User: NSObject, NSCoding{
    var id:Int?
    var userid:String?
    var givenName:String?
    var familyName:String?
    var email:String?
    var password:String?
    var steam:String?
    var origin:String?
    var image:String?
    var imageView:UIImage?
    
    init(id:Int?,userid:String?, givenName:String?, familyName:String?, email:String?,password:String?,steam:String?,origin:String?,image:String?,imageView:UIImage?){
        self.id = id
        self.userid = userid
        self.givenName = givenName
        self.familyName = familyName
        self.email = email
        self.password = password
        self.steam = steam
        self.image = image
        self.origin = origin
        self.imageView = imageView
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(userid, forKey: "Userid")
        aCoder.encode(givenName, forKey: "GivenName")
        aCoder.encode(familyName, forKey: "FamilyName")
        aCoder.encode(email, forKey: "Email")
        aCoder.encode(password, forKey: "Password")
        aCoder.encode(steam, forKey: "Steam")
        aCoder.encode(origin, forKey: "Origin")
        aCoder.encode(image, forKey: "Image")
        aCoder.encode(imageView, forKey: "ImageView")

    }
    
    required init?(coder: NSCoder) {
        id = coder.decodeObject(forKey:"id") as? Int
        userid = coder.decodeObject(forKey: "Userid") as? String
        givenName = coder.decodeObject(forKey: "GivenName") as? String
        familyName = coder.decodeObject(forKey: "FamilyName") as? String
        email = coder.decodeObject(forKey: "Email") as? String
        password = coder.decodeObject(forKey: "Password") as? String
        steam = coder.decodeObject(forKey: "Steam") as? String
        origin = coder.decodeObject(forKey: "Origin") as? String
        image = coder.decodeObject(forKey: "Image") as? String
        imageView = coder.decodeObject(forKey: "ImageView") as? UIImage
    }
    
    public static func getGameImage(_ urlImage:String, completion: @escaping (_ image: UIImage?, _ error:Error?) -> Void){
        
        let url = URL(string: urlImage)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                let image = #imageLiteral(resourceName: "icons8-customer-filled-100")
                completion(image, err)
                return
            }
            do {
                if let res = response as? HTTPURLResponse {
                    print("Downloaded picture with response code \(res.statusCode)")
                    if (res.statusCode != 200){
                        let image = #imageLiteral(resourceName: "icons8-customer-filled-100")
                        completion(image, err)
                        return
                    }
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        //print(image)
                        completion(image, err)
                    } else {
                        print("Couldn't get image: Image is nil")
                        completion(nil, err)
                    }
                } else {
                    print("Couldn't get response code for some reason")
                    completion(nil, err)
                }
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
}
