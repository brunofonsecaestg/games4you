//
//  Repositorio.swift
//  Games4YouAPP
//
//  Created by José Pedro Nascimento Guerra on 03/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation

class Repositorio:ProtocoloRepositorio {
    
    static let repositorio = Repositorio()
    
    fileprivate init() {}
    
    var jogos = [Game]()
    var users = [User]()
    
    var documentsPath:URL = URL(fileURLWithPath: "")
    var filePath:URL = URL(fileURLWithPath: "")
    
    //a chamar no inicio da app (didFinish)
    func obterJogo () {
        //unarchive
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("Jogo.data", isDirectory: false)
        let path = filePath.path
        
        print("loading games from \(path)")
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Game]{
            jogos = newData
            print("Loaded \(jogos.count) games")
            //            print(newData[0].nome!)
            //            print(newData[0].email!)
            //            print(newData[0].idade)
        }
    }
    
    func obterUser () {
        //unarchive
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("User.data", isDirectory: false)
        let path = filePath.path
        
        print("loading users from \(path)")
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [User]{
            users = newData
            print("Loaded \(users.count) users")
            //            print(newData[0].nome!)
            //            print(newData[0].email!)
            //            print(newData[0].idade)
        }
    }
    //a chamar sempre que houver alterações nas pessoas
    func guardarJogo (){
        //archive
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("Jogo.data", isDirectory: false)
        let path = filePath.path
        
        print("saving persons to \(path)")
        
        if NSKeyedArchiver.archiveRootObject(jogos, toFile: path){
            print("success saving games")
            print("Saved \(jogos.count) games")
        }
    }
    
    func guardarUser (){
        //archive
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("User.data", isDirectory: false)
        let path = filePath.path
        
        print("saving persons to \(path)")
        
        if NSKeyedArchiver.archiveRootObject(users, toFile: path){
            print("success saving users")
            print("Saved \(users.count) games")
        }
    }
    
    func removeGame() {
        
    }
    
    func removeUser() {
        
    }
    
}
