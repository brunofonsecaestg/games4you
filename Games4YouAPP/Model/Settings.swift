//
//  Settings.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 03/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation


struct Settings {
    
    struct IGDB {
        static let baseUrl = "https://api-2445582011268.apicast.io/"
        static let userKeyFieldName = "user-key"
        static let userKey = "7c8b49741a180f89226e3d678dc4cbd9"
        static let acceptHeaderFieldName = "Accept"
        static let acceptHeaderFieldValue = "application/json"
    }
    struct Auth {
       //static let baseUrlAuth = "http://13.81.118.230/Service1.svc/Rest/" //AZURE
        static let baseUrlAuth = "http://ec2-54-175-119-109.compute-1.amazonaws.com/Service1.svc/Rest/" //AMAZON
        //  static let baseUrlAuth = "http://192.168.1.72/Service1.svc/Rest/"
    }
    
    struct Twitch {
        static let baseUrl = "https://api.twitch.tv/kraken/"
        static let userKeyFieldName = "Client-ID"
        static let userKey = "uqmumigeocf6t0m5l5xz9faqaakhm7"
        static let acceptHeaderFieldName = "Accept"
        static let acceptHeaderFieldValue = "application/vnd.twitchtv.v5+json"
    }
    
}

