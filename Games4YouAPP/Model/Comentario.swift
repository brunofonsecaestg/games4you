//
//  Comentario.swift
//  Games4YouAPP
//
//  Created by Bruno José Fonseca on 06/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation

class Comentario {
    var id:Int?
    var text:String?
    var idUser:Int?
    var emailUser:String?
    
    init(id: Int,text: String,idUser:Int,emailUser:String) {
        self.id = id
        self.text = text
        self.idUser = idUser
        self.emailUser  = emailUser
    }
}
