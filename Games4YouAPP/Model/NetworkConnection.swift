//
//  NetworkConnection.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 05/12/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation
import Alamofire
import Foundation

class NetworkConnection {
    static let sharedInstance = NetworkConnection()
    
    func connectionHandler() {
        let manager = NetworkReachabilityManager(host: "www.google.pt")
        
        manager?.listener = { status in
            print("Network Status Changed: \(status)")
            if (NetworkConnection.isConnectedToInternet()) {
                print("Yes! internet is available.")
                // do some tasks..
            }else{
                print("No! internet is not available.")
            }
        }
        manager?.startListening()
    }
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
}

