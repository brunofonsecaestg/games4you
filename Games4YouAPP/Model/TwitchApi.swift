//
//  TwitchApi.swift
//  Games4YouAPP
//
//  Created by Bruno Anastácio on 16/11/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation
class TwitchApi{
    
    // sort by number of viewers, with limit streams
    public static func getStreamsByGameName(filter: String, completion: @escaping (_ channels: [String]?, _ error:Error?) -> Void) {
        let urlString = Settings.Twitch.baseUrl + "search/streams?query="+String(filter)+"&limit=3"
        let escapedAddress = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: escapedAddress!)
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.Twitch.userKey, forHTTPHeaderField: Settings.Twitch.userKeyFieldName)
        request.setValue(Settings.Twitch.acceptHeaderFieldValue, forHTTPHeaderField: Settings.Twitch.acceptHeaderFieldName)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        var channels:[String] = []
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
                if(jsonData!["streams"] != nil){
                    let json = jsonData!["streams"] as! [[String:Any]]
                    for item in json{
                        let channelInfo = item["channel"] as! [String:Any]
                        channels.append(channelInfo["name"] as! String)
                    }
                    completion(channels,err)
                }
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
}
