//
//  apiIgbd.swift
//  Games4YouAPP
//
//  Created by Admin on 31/10/2017.
//  Copyright © 2017 Games4You. All rights reserved.
//

import Foundation
class apiIgbd{
    var rating:String?
    var created_at:String?
    
    public static func getTop( completion: @escaping (_ games: [Game]?, _ error:Error?) -> Void) {
       
         let urlString = Settings.Auth.baseUrlAuth+"GetTopGames"
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.IGDB.userKey, forHTTPHeaderField: Settings.IGDB.userKeyFieldName)
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: Settings.IGDB.acceptHeaderFieldName)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        var games:[Game] = [];
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [[String:Any]]
                if(jsonData != nil){
                    for item in jsonData! {
                        var game = Game.parseJson(json: item)
                        games.append(game!)
                    }
                    completion(games,err)
                }
               
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    public static func getGameInfo(_ game:Int, completion: @escaping (_ game: Game?, _ error:Error?) -> Void){
        let urlString = Settings.IGDB.baseUrl + "games/"+String(game)
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.IGDB.userKey, forHTTPHeaderField: Settings.IGDB.userKeyFieldName)
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: Settings.IGDB.acceptHeaderFieldName)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [[String:Any]]
                if let gameJson = json?.first! {
                    let game = Game.parseJson(json: gameJson)
                    completion(game,err)
                }
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
    
    public static func getSearchedGames(_ filter:String, completion: @escaping (_ games: [Int]?, _ error:Error?) -> Void){
        let urlString = Settings.IGDB.baseUrl + "games/?search="+String(filter)
        let escapedAddress = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: escapedAddress!)
        var request = URLRequest(url: url!)
        
        request.setValue(Settings.IGDB.userKey, forHTTPHeaderField: Settings.IGDB.userKeyFieldName)
        request.setValue(Settings.IGDB.acceptHeaderFieldValue, forHTTPHeaderField: Settings.IGDB.acceptHeaderFieldName)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        var games:[Int] = [];
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, err) in
     
            if err != nil || data == nil {
                completion(nil, err)
                return
            }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [[String:Any]]
                for item in jsonData! {
                    games.append(item["id"] as! Int)
                }
                completion(games,err)
            }
            catch {
                completion(nil, err)
            }
        })
        dataTask.resume()
    }
    
}

